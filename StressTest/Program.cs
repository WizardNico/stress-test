﻿using AgileMQ.Bus;
using AgileMQ.Interfaces;
using StressTest.Directories.Ecommerce.Orders.Models;
using StressTest.Directories.Ecommerce.Products.Models;
using StressTest.Directories.Ecommerce.Products.Shared;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StressTest
{
	class Program
	{
		public static void Main(string[] args)
		{
			MainAsync(args);
		}

		public static async void MainAsync(string[] args)
		{
			using (IAgileBus bus = new RabbitMQBus("HostName=localhost;Port=5672;UserName=guest;Password=guest;Timeout=6;RetryLimit=3;PrefetchCount=100;AppId=StressTest"))
			{
				Console.WriteLine("Stress-Test Start....");

				for (int i = 0; i < 1000; i++)
				{
					//Creo un ordine
					OrderHeader msgHeader = new OrderHeader()
					{
						Note = "StressTest",
						State = Directories.Ecommerce.Orders.Enums.OrderState.New,
						ShippingAddressId = 99,
						UserId = 99
					};
					msgHeader = await bus.PostAsync(msgHeader);

					//Associo all'ordine 10 prodotti a caso
					for (int p = 0; p < 10; p++)
					{
						OrderRow msgRow = new OrderRow()
						{
							Quantity = new Random().Next(1, 50),
							OrderId = msgHeader.Id,
							ProductCode = "PRD_" + new Random().Next(1, 10),
							Options = new List<string>()
						};
						msgRow = await bus.PostAsync(msgRow);
					}

					//Confermo l'ordine
					msgHeader.State = Directories.Ecommerce.Orders.Enums.OrderState.Confirmed;
					await bus.PutAsync(msgHeader);
				}

				Console.WriteLine("Stress-Test completed with success!");
				Console.ReadLine();
			}
		}
	}
}
