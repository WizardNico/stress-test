﻿using AgileMQ.Attributes;
using StressTest.Directories.Ecommerce.Orders.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace StressTest.Directories.Ecommerce.Orders.Models
{
	[QueuesConfig(Directory = "Ecommerce", Subdirectory = "Orders", ResponseEnabled = true)]
	public class OrderHeader
	{
		[Required]
		public long? Id { get; set; }

		[Required]
		public OrderState? State { get; set; }

		[Required]
		public int? UserId { get; set; }

		public DateTime? CreationDate { get; set; }

		public DateTime? UpdateDate { get; set; }

		[Required]
		public int? ShippingAddressId { get; set; }

		[MaxLength(200)]
		public string Note { get; set; }

		public int? ItemsCount { get; set; }

		public double? TotalPrice { get; set; }
	}
}
